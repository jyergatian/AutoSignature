# AutoSignature

This script will read attributes from Active Directory (AD), place them into an HTML template, import HTML into Microsoft Outlook (2013 & 2016 tested).  A connection to AD is manditory for the information to populate.

Adjust the variables, supply an appicon, package as an App.  Your employees can now easily create Signatures for Outlook using data from within Active Directory or LDAP.

## To Do:

1. Introduce support for Apple's Mail.app
2. Move preferences to .plist
